# Prometheus - Configure Alerting for our Application 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Configure Monitoring Stack to notify us whenever CPU usage > 50% or Pod cannot start
  - Configure Alert Rules in Prometheus Server
  - Configure Alertmanager with Email Receiver

## Technologies Used 

* Prometheus 

* Kubernetes 

* Linux 

## Steps 

Step 1: Create file for alert rules configuration

[Create file for alert rules](/images/01%20create%20file%20for%20alert%20rules%20configuration.png)

Step 2: Insert name attribute to name of alert which in our case we want an alert that checks if we have a high cpu load HostHighCpuLoad

[Name attribute set](/images/02%20Insert%20name%20attribute%20to%20name%20alert%20which%20in%20our%20case%20we%20want%20an%20alert%20that%20checks%20if%20we%20have%20a%20high%20cpu%20load%20HostHighCpuLoad.png)

Step 3: Insert expression attriute which is conditional logic that checks if the cpu usage is over 50 percent send an alert

[expr attribute](/images/03%20insert%20expression%20attriute%20which%20is%20conditional%20logic%20which%20checks%20if%20the%20cpu%20usage%20is%20over%2050%20percent%20send%20an%20alert.png)

Step 4: Add for attribute which is to set when the alert is trigerred in this case put 2m

[for attribute](/images/04%20add%20for%20attribute%20which%20is%20to%20set%20when%20the%20alert%20is%20trigerred%20in%20this%20case%20put%202m%20.png)

Step 5: add labels attribute and set severity to warning because its only 50% cpu usage

[label severity](/images/05%20add%20labels%20attribute%20and%20add%20severity%20to%20warning%20because%20its%20only%2050%25%20cpu%20usage%20.png)

Step 6: Add anotation attribute with appropriate description and summary of alert

[Anotation attribute](/images/06%20Add%20anotation%20attribute%20with%20appropriate%20description%20and%20sumaary%20of%20alert.png)

Step 7: Add another labels attribute namespace and set it to monitoring which is the namespace where our prometheus stack is in

[label monitoring](/images/07%20Add%20another%20labels%20attribute%20namespace%20and%20set%20it%20to%20monitoring%20which%20is%20the%20namespace%20where%20our%20prometheus%20stack%20is%20in.png)

Step 8: Insert apiVersion in the alert rules file

[apiversion alert rule](/images/08%20insert%20apiVersion%20in%20the%20alert%20rules%20file.png)

Step 9: Insert kind to PrometheusRule because this is the custom resource name for alert rules in prometheus-stack

[alert rule kind](/images/09%20insert%20kind%20to%20PrometheusRule%20because%20this%20is%20the%20custom%20resource%20name%20for%20alert%20rules%20in%20prometheus-stack.png)

Step 10: Insert metadata information like the name and namespace you want this to be created

[alert rule metadata](/images/10%20Inser%20metadata%20information%20like%20the%20name%20and%20namespace%20you%20want%20this%20to%20be%20created%20.png)

Step 11: insert spec attribute with groups and in the group block insert necessary attributes like name and rules to alert which is the name of the alert we are creating

[alert rule spec](/images/11%20insert%20spec%20attribute%20with%20groups%20and%20in%20the%20group%20block%20necessary%20attributes%20like%20name%20and%20alert%20which%20is%20the%20name%20of%20the%20alert%20we%20are%20creating%20.png)

Step 12: Now move the alert configuration under the alert block

[alert configuration](/images/12%20Now%20move%20the%20alert%20configuration%20under%20the%20alert%20block.png)

Step 13: Create another alert specifically in situations in which the pod crashes to do this call alert attriute under spec groups rules with appropriate name you want it to have

[new alert](/images/13%20Lets%20create%20another%20alert%20specifically%20in%20situations%20in%20which%20the%20pod%20crashes%20to%20do%20this%20call%20alert%20attriute%20under%20spec%20groups%20rules%20with%20appropriate%20name%20you%20want%20it%20to%20have.png)

Note: If you go to prometheus ui you have a matrics called kube_pod_container_status_restarts_total which tells us how many times a container has restarted

Step 14: insert expr which has conditonal that checks if container restarts more than 5 times in a pod and if it does sends an alert use appropriate matrics and > 5

[pod expr](/images/15%20insert%20expr%20which%20has%20conditonal%20that%20if%20container%20restarts%20more%20than%205%20times%20in%20a%20pod%20send%20an%20alert%20use%20appropriate%20matrics%20and%20%3E%205.png)

Step 15: Add for attribute and insert when you want the alert to be trigerred, in this case immedately 0m

[podcrash for attribute](/images/16%20Add%20for%20attribute%20and%20insert%20when%20you%20want%20the%20alert%20to%20be%20trigerred%2C%20in%20this%20case%20immedately%200m%20%20.png)

Step 16: Add label attribute and in this attribute set severity to critical because its a more serious case and namespace to monitoring

[podcrash label severity](/images/17%20Add%20label%20attribute%20and%20in%20this%20attribute%20set%20severity%20to%20critical%20because%20its%20a%20more%20serious%20case%20and%20namespace%20to%20monitoring.png)

Step 17: Add annotation attribute with appropriate description and summary of alert

[podcrash annotation](/images/18%20Add%20annotation%20attribute%20with%20appropriate%20description%20and%20summary%20of%20alert.png)

Step 18: However for the prometheus operator to pick up our prometheus rules we need to add labels such as app kube-prometheus-stack and release monitoring because this labels will allow prometheus operators to be able to identify the prometheus rules that i want to add to prometheus and pick them up automatically

[label to allow prometheus operator identify rules](/images/19%20However%20for%20the%20prometheus%20operator%20to%20pick%20up%20our%20prometheus%20rules%20we%20need%20to%20add%20labels%20such%20as%20app%20kube-prometheus-stack%20and%20release%20monitoring%20because%20this%20labels%20will%20allow%20prometheus%20operators%20to%20be%20able%20to%20identify%20the%20prometheus%20rules%20that%20.png)

Step 19: Apply alert rule file

    kubectl apply -f alert-rules.yaml

[Apply alert rules](/images/20%20Apply%20alert%20rule%20file.png)
[Created rule](/images/21%20rule%20that%20was%20created%20main-rules.png)

Step 20: check logs of config reloader to check if it reloaded so the new rules can be recorded

    kubectl logs prometheus-monitoring-kube-prometheus-prometheus-0 -n monitoring -c config-reloader

[config reloader logs](/images/22%20check%20logs%20of%20config%20reloader%20to%20check%20if%20it%20reloaded%20so%20the%20new%20rules%20can%20be%20recorded.png)

[new alert rule on prometheus ui](/images/23%20new%20alert%20rules%20on%20prometheus%20ui.png)

Step 21: To test cpu alert pull a cpustress image

    kubectl run cpu-test --image=containerstack/cpustress -- --cpu 4 --timeout 30s --metrics-brief 

[cpustress image pull](/images/24%20to%20test%20cpu%20alert%20pull%20a%20cpustress%20image.png)

[Cpu over 50%](/images/25%20CPU%20over%2050%25.png)

Step 22: port forward alert  manager service to access through browser

    kubectl port-forward svc/monitoring-kube-prometheus-alertmanager -n monitoring 9093:9093 & 

[port-forward alertmanager](/images/25%20port%20forward%20alert%20%20manager%20service%20to%20access%20through%20browser.png)

[Alertmanager ui](/images/26%20alert%20manager%20ui.png)

[Alert pending and firing](/images/26%20Alert%20pending%20and%20firing.png)

Step 23: Create new file for alert manager configuration

[file for alert manager config](/images/27%20create%20new%20file%20for%20alert%20manager%20configuration.png)

Step 24: Insert appropriate apiVersion

[apiversion for alertmanager config](/images/28%20Insert%20appropriate%20apiVersion.png)

Step 25: Call kind attribute and insert appropriate kind AlertmanagrConfig

[alermanager config kind](/images/29_call%20kind%20attribute%20and%20insert%20appropriate%20kind%20AlertmanagrConfig.png)

Step 26: Call metadata attribute and use name you want the alertmanager component to be called and in the monitoring namespace

[alert manager metadata name and namespace](/images/30%20call%20metadata%20attribute%20and%20use%20name%20you%20want%20the%20alertmanager%20component%20to%20be%20called%20and%20in%20the%20monitoring%20namespace.png)

Step 27:  Add spec attribute under spec attribute add recievers with first attribute being name  so the appropriate name of reciever you want to use in our case email

[alert manager spec attribute reciever](/images/31%20add%20spec%20attribute%20under%20spec%20attribute%20add%20recievers%20with%20first%20attribute%20being%20name%20%20so%20the%20appropriate%20name%20of%20reciever%20you%20want%20to%20use%20in%20our%20case%20email.png)

Step 28: Add email config attribute next under reciever with to and from email address

[email config alert manager](/images/32%20add%20email%20config%20attribute%20next%20under%20reciever%20with%20to%20and%20from%20email%20address.png)

Step 29: Add smart host of the email provider you use under reciever_in my case it is gmail

[alert manager smart host](/images/33%20Add%20smart%20host%20of%20the%20email%20provider%20you%20use%20under%20reciever_in%20my%20case%20it%20is%20gmail%20.png)

Step 30: Add authUsername and authPassword attribute in reciever emailconfig block

[authUsername and authPassword](/images/34%20Add%20authUsername%20and%20aithPassword%20attribute%20in%20reciever%20emailconfig%20block.png)

Step 31: Create secret file to have you password in it rather than having it exposed in the alert manager configuration file

[Secret file creation](/images/35%20Create%20secret%20file%20to%20have%20you%20password%20in%20it%20rather%20than%20havingg%20it%20exposed%20in%20the%20aler%20manager%20configuration%20file.png)

Step 32: In secret file insert appropriate attributes like apiVersion_ kind which is Secret, type  = Opaque and finally metadata which will be the name you would want you secret component to have in the kubernetes cluster and finally name space monitoring

[Secret file](/images/36%20in%20secret%20file%20insert%20appropriate%20attributes%20like%20apiVerssion_%20kind%20which%20is%20secre_type%20Opaque%20and%20finally%20metadata%20which%20will%20be%20the%20name%20you%20would%20want%20you%20secret%20component%20to%20have%20in%20the%20kubernetes%20cluster%20and%20finally%20name%20space%20monitoring%20.png)

Step 33: Add data attribute with password being base 64 encoded

[data attribute for secret](/images/37%20add%20data%20attribute%20with%20password%20being%20base%2064%20encoded.png)

Step 34: For security reasons get app password from your gmail and base 64 encode it

[gmail password](/images/38%20for%20security%20reasons%20get%20app%20password%20from%20your%20gmail%20and%20base%2064%20encode%20it%20%5D.png)

Step 35: In alertmanager configuration file reference secret file in authpassword and data which will contain your base 64 encoded password

[Referencing secret file as password](/images/39%20in%20alertmanager%20configuration%20file%20reference%20secret%20file%20in%20authpassword%20and%20data%20which%20will%20contain%20your%20base%2064%20encoded%20password.png)

Step 36: Add authIdentity attribute in emailConfigs block

[authIdentity](/images/40%20add%20authIdentity%20attribute%20in%20emailConfigs%20block.png)

Step 37: Route needs to be configured next so in spec attribute block call route which has attribute called routes, under routes call matchers  with appropriate name in this case alertname and next value which should be the name of the alert 

[Route](/images/41%20Route%20needs%20to%20be%20configured%20next%20so%20in%20spec%20attribute%20block%20call%20route%20which%20has%20attribute%20called%20routes_%20under%20routes%20call%20matchers%20%20with%20appropriate%20name%20in%20this%20case%20alertname%20and%20next%20value%20which%20should%20be%20the%20name%20of%20the%20alert.png)

Step 38: Add second matchers configuration for the second alert

[Second alert matcher](/images/42%20add%20second%20matchers%20configuration%20for%20the%20second%20alert%20.png)

Step 39:  Add reciever in route block which will enable the two alert send to the email given in the reciever configuration,this is possible due to the matchers attribute

[Reciever](/images/43%20add%20reciever%20in%20route%20block%20which%20will%20enable%20the%20two%20alert%20send%20to%20the%20email%20given%20in%20the%20reciever%20configuration_this%20is%20%20possible%20due%20to%20the%20matchers%20attribute.png)

Step 40: in the route for kubernetesPodCrahLooping  set repeatInterval attribute to 30 minutes this is because this alert is a critical one

[RepeatInterval](/images/44%20in%20the%20route%20for%20kubernetesPodCrahLooping%20we%20an%20set%20repeatIntrval%20attribute%20to%2030%20minutes%20this%20is%20because%20this%20alert%20is%20a%20critical%20one.png)

Step 41: apply the two files with kubectl

    kubectl apply -f email-secret.yaml
    kubectl apply -f alert-manager-configuration.yaml 

[config triggered](/images/46%20configuration%20was%20triggered.png)
[Apply 2 files](/images/45%20apply%20the%20two%20files%20with%20kubectl.png)
[configuration merged](/images/47%20existing%20configuration%20merged%20with%20new%20configuration%20on%20alert%20manager%20UI.png)

Step 42: Test Alert

    kubectl run cpu-test --image=containerstack/cpustress -- --cpu 4 --timeout 30s --metrics-brief 

[testing alert](/images/48%20test%20alert.png)
[All alerts fired](/images/49%20all%20alerts%20that%20has%20been%20fired.png)
[email recieved](/images/50%20email%20i%20recieved.png)
[second alert fired](/images/51_second_alert_firing.png)
[second email recieved](/images/52_second_alert_email.png)


## Installation

    brew install eksctl 

## Usage 

    kubectl apply -f email-secret.yaml
    kubectl apply -f alert-manager-configuration.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/prometheus-configure-alerting-for-our-application.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/prometheus-configure-alerting-for-our-application

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.